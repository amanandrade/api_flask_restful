# Projeto de REST API com Flask-RESTful

Atividade desenvolvida no curso da Digital Innovation One

### Exemplos com um banco de dados simulado. 
* *app.py* contém um rest api sem o flask-restful.
* *app_restful.py* contém o mesmo exemplo de rest api, mas usando o flask-restful.